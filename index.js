const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')
        
    var self = this

    _.forEach(self.prototype.requiredArguments, function(v, k){
        if(!(args[k] instanceof Element))
            throw new Error('Invalid argument ' + args[k] + '. An element instance is expected')
        
        self[k] = args[k]
    })

    self.updateMessage = function(message){
        message = typeof message !== 'string' ? 'Cargando...' : message
        self.messageElement.innerHTML = message
        return self
    }

    self.hideSpinner = function(){
        self.spinner.style.display = 'none'
        return self
    }

    self.updateIcon = function(icon){
        iconString = typeof icon !== 'string' ? '' : icon
        self.messageIcon.innerHTML = iconString
        return self
    }

    self.hide = function(){
        self.element.style.opacity = 0
        setTimeout(function(){
            self.element.style.display = 'none'
        }, 550)
    }
}

module.exports.prototype.requiredArguments = {
    messageElement: true,
    messageIcon: true,
    spinner: true,
    element: true,
}